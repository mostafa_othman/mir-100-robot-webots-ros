"""mir_controller controller."""

# You may need to import some classes of the controller module. Ex:
#  from controller import Robot, Motor, DistanceSensor
from controller import Robot

from std_msgs.msg import Float32MultiArray
from sensor_msgs.msg import Imu, Image
from geometry_msgs.msg import Twist
import numpy as np
from cv_bridge import CvBridge
import rospy

HALF_DISTANCE_BETWEEN_WHEELS = 0.222604/2
WHEEL_RADIUS = 0.0465

class RobotController():
    """class RobotController """
    def __init__(self) -> None:
        self.bridge = CvBridge()
        self.robot = Robot()
        self.robot_name = self.robot.getName()
        self.timestep = int(self.robot.getBasicTimeStep())
        # Sensors
        self.sensor_names = ['imu_data accelerometer', 'imu_data gyro', 'imu_data compass','ds0', 'ds1', 'ds2', 'ds3','depth_camera', 'middle_left_wheel_joint_sensor', 'middle_right_wheel_joint_sensor', 'front_left_caster_joint_sensor', 'front_left_wheel_joint_sensor', 'front_right_caster_joint_sensor', 'front_right_wheel_joint_sensor', 'back_left_caster_joint_sensor', 'back_left_wheel_joint_sensor', 'back_right_caster_joint_sensor', 'back_right_wheel_joint_sensor']
        self.all_sensors = []
        self.enable_sensors()
        self.depth_camera = self.robot.getDevice("depth_camera")
        self.front_left_caster_sensor = self.robot.getDevice("front_left_caster_joint_sensor")
        self.front_right_caster_sensor = self.robot.getDevice("front_right_caster_joint_sensor")
        self.middle_left_wheel_sensor = self.robot.getDevice("middle_left_wheel_joint_sensor")
        self.middle_right_wheel_sensor = self.robot.getDevice("middle_right_wheel_joint_sensor")
        self.back_left_caster_sensor = self.robot.getDevice("back_left_caster_joint_sensor")
        self.back_right_caster_sensor = self.robot.getDevice("back_right_caster_joint_sensor")
        self.back_left_wheel_sensor = self.robot.getDevice("back_left_wheel_joint_sensor")
        self.back_right_wheel_sensor = self.robot.getDevice("back_right_wheel_joint_sensor")
        # Actors
        self.left_motor = self.robot.getDevice("middle_left_wheel_joint")
        self.right_motor = self.robot.getDevice("middle_right_wheel_joint")
        self.right_motor.setPosition(float("inf"))
        self.right_motor.setVelocity(0)
        self.left_motor.setPosition(float("inf"))
        self.left_motor.setVelocity(0)

        self.__target_twist = Twist()
        print(self.right_motor.LINEAR)
        
        # print(dir(self.robot.getDevice("middle_left_wheel_joint_sensor")))
        
    
    def enable_sensors(self):
        """ 
        ### Enable All Sensors
        - Only sensor devices in the `robot`.
        """
        for sensor_name in self.sensor_names:
            self.robot.getDevice(sensor_name).enable(self.timestep)
            self.all_sensors.append(self.robot.getDevice(sensor_name))

    def run(self):
        """
        ### Start run the robot controller
        """
        while self.robot.step(self.timestep) != -1:
            self.publish_ros_messages()
            self.right_motor.setPosition(float("inf"))
            self.right_motor.setVelocity(0)
            # print(self.robot.getDevice("middle_left_wheel_joint_sensor").getValue())

            forward_speed = self.__target_twist.linear.x
            angular_speed = self.__target_twist.angular.z
            command_motor_left = (forward_speed - angular_speed * HALF_DISTANCE_BETWEEN_WHEELS) / WHEEL_RADIUS
            command_motor_right = (forward_speed + angular_speed * HALF_DISTANCE_BETWEEN_WHEELS) / WHEEL_RADIUS

            self.left_motor.setVelocity(command_motor_left)
            self.right_motor.setVelocity(command_motor_right)
            pass
    

    def publish_ros_messages(self):
        rospy.init_node(self.robot.getName())
        self.distance_sensors_pub = rospy.Publisher("/" + self.robot_name + "/distance_sensors", Float32MultiArray, queue_size=10)
        self.imu_pub = rospy.Publisher("/" + self.robot_name + "/imu", Imu, queue_size=10)
        self.depth_pub = rospy.Publisher("/" + self.robot_name + "/depth", Image, queue_size=10)
        self.wheels_pub = rospy.Publisher("/" + self.robot_name + "/wheels_sensors", Float32MultiArray, queue_size=10)
        self.cmd_vel = rospy.Subscriber("/" + self.robot_name + "/cmd_vel",Twist, self.__cmd_vel_callback, 1)

        self.publish_distance_sensors_msgs()
        self.publish_imu_msgs()
        self.publish_depth_msgs()
        self.publish_wheels_msgs()
        

    def publish_distance_sensors_msgs(self):
        distance_sensors_data = Float32MultiArray()
        distance_sensors_data.data = [self.all_sensors[3].getValue(), self.all_sensors[4].getValue(), self.all_sensors[5].getValue(), self.all_sensors[6].getValue()]
        self.distance_sensors_pub.publish(distance_sensors_data)

    def publish_imu_msgs(self):
        imu_data = Imu()
        imu_data.header.frame_id = "imu"
        imu_data.header.stamp = rospy.Time.now()
        imu_data.header.seq = self.timestep
        imu_data.linear_acceleration.x = self.robot.getDevice("imu_data accelerometer").getValues()[0]
        imu_data.linear_acceleration.y = self.robot.getDevice("imu_data accelerometer").getValues()[1]
        imu_data.linear_acceleration.z = self.robot.getDevice("imu_data accelerometer").getValues()[2]
        imu_data.angular_velocity.x = self.robot.getDevice("imu_data gyro").getValues()[0]
        imu_data.angular_velocity.y = self.robot.getDevice("imu_data gyro").getValues()[1]
        imu_data.angular_velocity.z = self.robot.getDevice("imu_data gyro").getValues()[2]
        imu_data.orientation.x = self.robot.getDevice("imu_data compass").getValues()[0]
        imu_data.orientation.y = self.robot.getDevice("imu_data compass").getValues()[1]
        imu_data.orientation.z = self.robot.getDevice("imu_data compass").getValues()[2]
        self.imu_pub.publish(imu_data)

    def publish_depth_msgs(self):
        depth_image_data = self.depth_camera.getRangeImageArray()
        min_range = self.depth_camera.getMinRange()
        max_range = self.depth_camera.getMaxRange()
        depth_image_data_clipped = np.clip(depth_image_data, min_range, max_range)
        depth_image_data_normalized = (depth_image_data_clipped - min_range) / (max_range - min_range)
        scaled_depth_image = (depth_image_data_normalized * 255).astype(np.uint8)
        ros_depth_image = self.bridge.cv2_to_imgmsg(scaled_depth_image)
        self.depth_pub.publish(ros_depth_image)

    def publish_wheels_msgs(self):
        wheels_data = Float32MultiArray()
        wheels_data.data = [self.front_left_caster_sensor.getValue(), self.front_right_caster_sensor.getValue(), self.middle_left_wheel_sensor.getValue(), self.middle_right_wheel_sensor.getValue(), self.back_left_caster_sensor.getValue(), self.back_right_caster_sensor.getValue(), self.back_left_wheel_sensor.getValue(), self.back_right_wheel_sensor.getValue()]
        self.wheels_pub.publish(wheels_data)

    def __cmd_vel_callback(self, twist, o):
        self.__target_twist = twist

    # def convert_depth_image(self, depth_image_data, max_range, min_range):
    #     depth_image_data_clipped = np.clip(depth_image_data, min_range, max_range)

    #     # Normalize depth values within the valid range
    #     depth_image_data_normalized = (depth_image_data_clipped - min_range) / (max_range - min_range)

    #     # Scale the normalized values to fit into 8-bit depth range
    #     scaled_depth_image = (depth_image_data_normalized * 255).astype(np.uint8)

    #     # Return the scaled depth image
    #     return scaled_depth_image

if __name__ == "__main__":
    robot = RobotController()
    robot.run()


